#include "Frame.h"

using namespace cerberus::ui;

namespace {

wxAuiPaneInfo DefaultChatListPaneInfo()
{
	return wxAuiPaneInfo()
		.Center()
		.CaptionVisible(false)
		.CloseButton(false);
}

wxAuiPaneInfo DefaultStatusListPaneInfo()
{
	return wxAuiPaneInfo()
		.Bottom()
		.PinButton(true);
}

wxAuiPaneInfo DefaultChannelTreePaneInfo()
{
	return wxAuiPaneInfo()
		.Caption(wxT("Channels"))
		.Left();
}

wxAuiPaneInfo DefaultUserTreePaneInfo()
{
	return wxAuiPaneInfo()
		.Right()
		.PinButton(true)
		.Hide();
}

}

Frame::Frame(
  wxWindow* parent,
  wxWindowID id,
  const wxString& title,
  const wxPoint& pos,
  const wxSize& size,
  long style)
: wxFrame(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);
	m_mgr.SetManagedWindow(this);
	m_mgr.SetFlags(wxAUI_MGR_DEFAULT);

	chat_list = new wxListCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_ICON);
	m_mgr.AddPane(chat_list, DefaultChatListPaneInfo());

	status_list = new wxListCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_ICON);
	m_mgr.AddPane(status_list, DefaultStatusListPaneInfo());

	channel_tree = new wxTreeCtrl( this, wxID_ANY, wxDefaultPosition, wxSize(150, -1), wxTR_DEFAULT_STYLE);
	m_mgr.AddPane(channel_tree, DefaultChannelTreePaneInfo());

	user_tree = new wxTreeCtrl(this, wxID_ANY, wxDefaultPosition, wxSize(120, -1), wxTR_DEFAULT_STYLE);
	m_mgr.AddPane(user_tree, DefaultUserTreePaneInfo());

	m_mgr.Update();
	this->Centre(wxBOTH);
}

Frame::~Frame()
{
	m_mgr.UnInit();
}
