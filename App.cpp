#include "Frame.h"
#include <wx/app.h>

namespace cerberus {
namespace ui {

struct App : public wxApp {
	Frame *m_frame;

	virtual bool OnInit();
	virtual int OnExit();
};

bool App::OnInit()
{
	m_frame = new Frame(NULL);
	m_frame->Show();
	return true;
}

int App::OnExit()
{
	delete m_frame;
	return 0;
}

}
}

wxIMPLEMENT_APP(cerberus::ui::App);