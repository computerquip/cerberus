#pragma once

#include <wx/aui/aui.h>
#include <wx/frame.h>
#include <wx/listctrl.h>
#include <wx/treectrl.h>

namespace cerberus {
namespace ui {

class Frame : public wxFrame
{
	private:
		wxAuiManager m_mgr;

	protected:
		wxListCtrl* chat_list;
		wxListCtrl* status_list;
		wxTreeCtrl* channel_tree;
		wxTreeCtrl* user_tree;

	public:
		Frame(
		  wxWindow* parent,
		  wxWindowID id = wxID_ANY,
		  const wxString& title = wxT("Cerberus IRC"),
		  const wxPoint& pos = wxDefaultPosition,
		  const wxSize& size = wxSize( 912,431 ),
		  long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL
		);

		~Frame();
};


}
}

